<header itemscope itemtype="http://schema.org/Organization">

    
    <div class="container">
        <div class="topo">
            <div class="col-md-10">
                <div class="tel">
                    <a title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>">
                        <i class="fas fa-phone-alt"></i><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["telefone"]; ?></span>
                    </a> 
                    |
                    <a title="whatsApp" href="">
                        <i class="fab fa-whatsapp"></i><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["whatsapp"]; ?></span>
                    </a> 
                    |
                    <a title="E-mail" href="mailto:<?php echo $emailContato; ?>">
                        <i class="fas fa-envelope-open-text"></i><span itemprop="telephone"> <?php echo $emailContato; ?></span>
                    </a> 
                </div>
            </div>
            <div class="col-md-2">
                <div class="redes-topo">
                    <a href="" target="_blank" title="Facebook <?php echo $nome_empresa; ?>"><i class="fab fa-facebook-f"></i></a>
                    <a href="" target="_blank" title="Instagram <?php echo $nome_empresa; ?>"><i class="fab fa-instagram"></i></a>
                    <a href="" target="_blank" title="Youtube <?php echo $nome_empresa; ?>"><i class="fab fa-youtube"></i></a>
                    <a href="" target="_blank" title="Linkedin <?php echo $nome_empresa; ?>"><i class="fab fa-linkedin-in"></i></a> 
                </div>
            </div>
        </div>
    </div>  

    <div class="container header-container-main">
        <div class="col-md-4">
            <div class="logo">
                <a href="<?php echo $url; ?>" title="<?php echo $h1 . " - " . $nome_empresa; ?>">
                    <span itemprop="image">
                        <img src="<?php echo $url; ?>imagens/logo.png" alt="<?php echo $nome_empresa; ?>" title="<?php echo $nome_empresa; ?>" class="img-responsive">
                    </span>
                </a>
            </div>
        </div>
        
        <div class="col-md-8">
            <nav class="menu">
                <ul class="menu-list">
                    <li><a href="http://mondialmodulares.com.br/" title="Página inicial">Home</a></li>
                    <li><a href="http://mondialmodulares.com.br/a-empresa/" title="A Empresa">A Empresa</a></li>
                    <!-- <li><a href="http://mondialmodulares.com.br/construcao-modular/" title="Contstrução Modular">Contstrução Modular</a></li> -->
                    <!-- <li><a href="http://mondialmodulares.com.br/portfolio/" title="Módulos">Módulos</a></li> -->
                    <!-- <li><a href="http://mondialmodulares.com.br/especificacoes/" title="Especificações">Especificações</a></li> -->
                    <!-- <li><a href="http://www.mondialwindows.com.br/" title="Esquadrias">Esquadrias</a></li> -->
                    <!-- <li><a href="hhttp://mondialmodulares.com.br/folder-eletronico/" title="Folder Eletrônico">Folder Eletrônico</a></li> -->
                    <li><a href="http://mondialmodulares.com.br/solicite-um-orcamento/" title="Solicite um Orçamento">Solicite um Orçamento</a></li>
                    <li><a href="<?php echo $url; ?>" title="Informações">Informações</a></li>
                    <li><a href="http://mondialmodulares.com.br/#contato" title="Contato">Contato</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>