<?php

    // Principais Dados do Cliente
$nome_empresa = "Mondial Modulares";
$emailContato = "contato@mondialmodulares.com.br";

    // Parâmetros de Unidade
$unidades = array(
    1 => array(
        "nome" => "Mondial Modulares",
        "rua" => "Rod. BR 101 - nº01",
        "bairro" => "Taborda",
        "cidade" => "São José de Mipibu ",
        "estado" => "Rio Grande do Norte",
        "uf" => "RN",
        "cep" => "59162-000",
            "latitude_longitude" => "", // Consultar no maps.google.com
            "ddd" => "84",
            "telefone" => "98702-9374",
            "whatsapp" => "98702-9374",
            "link_maps" => "" // Incorporar link do maps.google.com
        ),
    2 => array(
        "nome" => "",
        "rua" => "",
        "bairro" => "",
        "cidade" => "",
        "estado" => "",
        "uf" => "",
        "cep" => "",
        "ddd" => "",
        "telefone" => ""
    )
);

    // Parâmetros para URL
$padrao = new classPadrao(array(
        // URL local
    "http://localhost/informacoes.mondialmodulares.com.br/",
        // URL online
    "https://www.informacoes.mondialmodulares.com.br/"
));

    // Variáveis da head.php
$url = $padrao->url;
$canonical = $padrao->canonical;

    // Parâmetros para Formulário de Contato
    $smtp_contato            = ""; // Solicitar ao líder do dpto técnico, 177.85.98.119
    $email_remetente         = ""; // Criar no painel de hospedagem, admin@...
    $senha_remetente         = "c0B1S3vH5eCvAO";

    // Contato Genérico (para sites que não se hospedam os e-mails)
    // $smtp_contato            = "111.111.111.111";
    // $email_remetente         = "formulario@temporario-clientes.com.br";
    // $senha_remetente         = "4567FGHJK";

    // Recaptcha Google
    $captcha                 = false; // https://www.google.com/recaptcha/
    $captcha_key_client_side = "";
    $captcha_key_server_side = "";

    // CSS default
    $padrao->css_files_default = array(
        "default/reset",
        "default/grid-system",
        "default/main",
        "default/slicknav-menu",
        "_main-style"
    );
    
    // JS Default
    $padrao->js_files_default = array(
        "default/jquery-1.9.1.min",
        "default/modernizr",
        "default/jquery.slicknav.min",
        "jquery.padrao.main"
    );

    // Listas de Palavras Chave
    $palavras_chave = array(
        "Venda de containers",
        "Comprar containers",
        "Container a venda",
        "Construção modular",
        "Preço de container",
        "Módulo habitável",
        "Empresa de container",
        "Container grande",
        "Container pequeno",
        "Valores de containers",
        "Container para obra",
        "Fábrica de container",
        "Fabricação de container",
        "Fabricante de container",
        "Construção de container",
        "Container para comprar",
        "Container modular",
        "Aluguel de container para obra",
        "Aluguel de container pequeno",
        "Aluguel de container grande",
        "Construção em container",
        "Container para escritório",
        "Container para escritório valores",
        "Container para alugar",
        "Aluguel de container",
        "Escritório de container",
        "Container com banheiro",
        "Container 6 metros",
        "Container alojamento",
        "Custo de um container",
        "Valor do metro container",
        "Container com estrutura de aço",
        "Container com isolamento acústico",
        "Container com isolamento térmico",
        "Projetos personalizados de containers",
        "Projetos provisórios de containers",
        "Projetos definitivos de containers"
    );

    $palavras_chave_com_descricao = array(
        "Item 1" => "Lorem ipsum dolor sit amet.",
        "Item 2" => "Laudem dissentiunt ut per.",
        "Item 3" => "Solum repudiare dissentiunt at qui.",
        "Item 4" => "His at nobis placerat.",
        "Item 5" => "Ei justo lucilius nominati vim."
    );
    
     /**
     * Submenu
     * 
     * $opcoes = array(
     * "id" => "",
     * "class" => "",
     * "limit" => 9999,
     * "random" => false
     * );
     * 
     * $padrao->subMenu($palavras_chave, $opcoes);
     * 
     */

    /**
     * Breadcrumb
     * 
     * -> Propriedades
     * 
     * Altera a url da Home no breadcrumb
     * $padrao->breadcrumb_url_home = "";
     * 
     * Altera o texto que antecede a Home
     * $padrao->breadcrumb_text_before_home = "";
     * 
     * Altera o texto da Home no breadcrumb
     * $padrao->breadcrumb_text_home = "Home";
     * 
     * Altera o divisor de níveis do breadcrumb
     * $padrao->breadcrumb_spacer = " » ";
     * 
     * -> Função
     * 
     * Cria o breadcrumb
     * $padrao->breadcrumb(array("Informações", $h1));
     * 
     */

    /**
     * Lista Thumbs
     * 
     * $opcoes = array(
     * "id" => "",
     * "class_div" => "col-md-3",
     * "class_section" => "",
     * "class_img" => "img-responsive",
     * "title_tag" => "h2",
     * "folder_img" => "imagens/thumbs/",
     * "extension" => "jpg",
     * "limit" => 9999,
     * "type" => 1,
     * "random" => false,
     * "text" => "",
     * "headline_text" => "Veja Mais"
     * );
     * 
     * $padrao->listaThumbs($palavras_chave, $opcoes);
     * 
     */
    
    /**
     * Funções Extras
     * 
     * $padrao->formatStringToURL();
     * Reescreve um texto em uma URL válida
     * 
     */