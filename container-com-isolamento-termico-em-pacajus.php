<?php
$title       = "Container com isolamento térmico em Pacajus";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Para oferecer sempre o melhor Container com isolamento térmico em Pacajus do mercado, a Mondial Modulares utiliza materiais de alta qualidade e excelente durabilidade no processo de fabricação deste produto, assim, o cliente que compra com a nossa empresa tem a garantia que irá receber o melhor container para seu ambiente, entre em contato e compre o seu com a melhor empresa.</p>
<p>A Mondial Modulares é uma empresa que se destaca no setor de Modulares, pois oferece não somente Container 6 metros, Container com isolamento térmico, Aluguel de container grande, Container para obra e Comprar containers, mas também, Container com isolamento térmico em Pacajus com o intuito de atender às mais exigentes solicitações. Entre em contato e faça uma cotação com um de nossos competentes profissionais e assim comprove que somos a empresa que trabalha com o foco na qualidade e satisfação de nossos clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>