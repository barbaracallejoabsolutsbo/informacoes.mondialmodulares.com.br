<?php
$title       = "Container com banheiro em Baraúna - RN";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você está procurando uma empresa que faça a melhor venda de Container com banheiro em Baraúna - RN, está no lugar certo, a Mondial Modulares trabalha neste ramo desde 2011, por isso, temos experiência de sobra quando falamos neste assunto, além disso, todos os nossos containers são fabricados com materiais de excelente qualidade e possuem uma ótima durabilidade.</p>
<p>Você procura por Container com banheiro em Baraúna - RN? Contar com empresas especializadas no segmento de Modulares é sempre a melhor saída, já que assim temos a segurança de excelência e profissionalismo. Pensando assim, a empresa Mondial Modulares é a opção certa para quem busca a soma de qualidade, comprometimento e agilidade em Container com isolamento acústico, Fabricante de container, Projetos provisórios de containers, Container 6 metros e Aluguel de container grande e ainda, um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>