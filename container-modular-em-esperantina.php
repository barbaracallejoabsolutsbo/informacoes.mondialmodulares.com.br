<?php
$title       = "Container modular em Esperantina";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Quando um cliente decide realizar a compra de um Container modular em Esperantina, ele terá muitos benefícios, como rapidez de execução, redução de custos, garantia de qualidade, flexibilidade e sustentabilidade. Para obter todos esses benefícios, entre em contato com nossa empresa, somos os melhores do mercado, além disso, temos o melhor valor e condições de pagamento muito boas.</p>
<p>Se está procurando por Container modular em Esperantina e prioriza empresas idôneas e com os melhores profissionais para o seu atendimento, a Mondial Modulares é a melhor opção do mercado. Unindo profissionais com alto nível de experiência no segmento de Modulares conseguem oferecer soluções diferenciadas para garantir o objetivo de cada cliente quando falamos de Empresa de container, Comprar containers, Container para obra, Container com isolamento térmico e Container para escritório valores.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>