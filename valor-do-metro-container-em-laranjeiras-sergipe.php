<?php
$title       = "Valor do metro container em Laranjeiras - Sergipe";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Entregando o melhor container para os clientes desde 20111, a Mondial Modulares também conta com o melhor Valor do metro container em Laranjeiras - Sergipe, isso que significa que comprando este produto com a nossa empresa o cliente irá adquirir um container de excelente qualidade com uma ótima durabilidade por um valor baixo e ótimas condições de pagamento, entre em contato e compre o seu.</p>
<p>Nós da Mondial Modulares trabalhamos dia a dia para garantir o melhor em Valor do metro container em Laranjeiras - Sergipe e para isso contamos com profissionais altamente capacitados para atender e garantir a satisfação de seus clientes e parceiros. Atuando no mercado de Modulares com qualidade e dedicação, contamos com profissionais com amplo conhecimento em Container com isolamento térmico, Construção modular, Container com estrutura de aço, Comprar containers e Container com isolamento acústico e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>