<?php
$title       = "Container a venda em Jequié";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O melhor container para escritório você encontra em nossa empresa, para isso, a Mondial Modulares trabalha com os melhores produtos deste segmento, além disso, contamos com valores que cabem no bolso e ótimas condições de pagamento, não perca tempo, entre em contato conosco agora mesmo e compre o melhor container do mercado, estamos esperando por você.</p>
<p>Trabalhando há anos no mercado voltado à Modulares, a Mondial Modulares é uma empresa de grande experiente e qualificada quando se trata de Comprar containers, Módulo habitável, Container para comprar, Container com isolamento acústico e Preço de container e Container a venda em Jequié. Em constante busca da satisfação de nossos clientes, atuamos no mercado com o intuito de fornecer para todos que buscam pela nossa qualidade o que há de mais moderno e eficiente no segmento.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>