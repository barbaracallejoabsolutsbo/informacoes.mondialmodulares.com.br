<?php
$title       = "Construção modular em São Cristóvão - Sergipe";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Contando com uma experiência de mais de 10 anos em Construção modular em São Cristóvão - Sergipe, a Mondial Modulares é a empresa certa para você realizar este serviço, para isso, contamos com os melhores profissionais do ramo que são altamente qualificados para realizar um serviço e atendimento de excelente qualidade, auxiliando também em todas as etapas deste processo, entre em contato conosco para saber todas as informações sobre este serviço que oferecemos.</p>
<p>Atuando de forma a cumprir os padrões de qualidade do mercado de Modulares, a Mondial Modulares é uma empresa experiente quando se trata do ramo de Container com isolamento térmico, Container para escritório, Container 6 metros, Container para comprar e Projetos personalizados de containers. Por isso, se você busca o melhor com um custo acessível e vantajoso em Construção modular em São Cristóvão - Sergipe aqui você encontra o que precisa sem a diminuição da qualidade. Busque sempre o melhor para ter uma real satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>