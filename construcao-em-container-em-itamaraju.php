<?php
$title       = "Construção em container em Itamaraju";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Quando realizamos a Construção em container em Itamaraju, devemos considerar alguns processos que não podem ser perdidos, como testes de segurança. A Mondial Modulares segue os padrões de segurança desde a sua fundação, por isso oferecemos contêineres de alta qualidade, durabilidade de alto nível e segurança garantida, entre em contato e compre já o seu.</p>
<p>Com credibilidade no mercado de Modulares, a Mondial Modulares trabalha dia a dia com foco em proporcionar com qualidade, viabilidade e custo x benefício acessível tanto Valor do metro container, Container 6 metros, Comprar containers, Container com isolamento térmico e Empresa de container quanto em Construção em container em Itamaraju. Por isso, se você busca pelo melhor para você, conte com a nossa equipe de profissionais altamente capacitados, garantindo assim seu sucesso no mercado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>