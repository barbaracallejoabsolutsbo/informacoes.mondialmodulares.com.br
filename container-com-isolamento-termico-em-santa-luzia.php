<?php
$title       = "Container com isolamento térmico em Santa Luzia";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Para oferecer sempre o melhor Container com isolamento térmico em Santa Luzia do mercado, a Mondial Modulares utiliza materiais de alta qualidade e excelente durabilidade no processo de fabricação deste produto, assim, o cliente que compra com a nossa empresa tem a garantia que irá receber o melhor container para seu ambiente, entre em contato e compre o seu com a melhor empresa.</p>
<p>A Mondial Modulares, como uma empresa em constante desenvolvimento quando se trata do mercado de Modulares visa trazer o melhor resultado em Container com isolamento térmico em Santa Luzia para todos os clientes que buscam uma empresa de confiança e competência. Contamos com profissionais com amplo conhecimento em Container para escritório, Aluguel de container para obra, Fábrica de container, Venda de containers e Aluguel de container para levar sempre o melhor para você, garantindo assim a sua satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>