<?php
$title       = "Comprar containers em Balsas";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Para Comprar containers em Balsas de excelente qualidade é necessário alguns requisitos que você deve se preocupar, procurar uma empresa com experiência e que seja de alto nível, produtos de excelente qualidade, preços que cabem no bolso e ótimas condições de pagamento, tempo de entrega, entre outros requisitos, tudo isso você encontra na melhor loja do segmento.</p>
<p>Se está procurando por Comprar containers em Balsas e prioriza empresas idôneas e com os melhores profissionais para o seu atendimento, a Mondial Modulares é a melhor opção do mercado. Unindo profissionais com alto nível de experiência no segmento de Modulares conseguem oferecer soluções diferenciadas para garantir o objetivo de cada cliente quando falamos de Aluguel de container pequeno, Escritório de container, Container modular, Construção em container e Fabricação de container.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>