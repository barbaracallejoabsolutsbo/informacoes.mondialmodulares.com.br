<?php
$title       = "Container para escritório em Recife";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O melhor Container para escritório em Recife valores baixos e ótimas condições de pagamento, você encontra na Mondial Modulares, nossa empresa atua desde 2011 neste ramo, por isso, temos experiência de sobra na realização deste serviço, além disso, em todos os processos de fabricação deste produto, usamos materiais de excelente qualidade e ótima durabilidade.</p>
<p>Na busca por uma empresa qualificada em Modulares, a Mondial Modulares será sempre a escolha com as melhores vantagens para o que você vem buscando. Além de fornecedor Aluguel de container, Construção de container, Custo de um container, Valor do metro container e Container pequeno com o melhor custo x benefício da região, nós temos como missão garantir agilidade, qualidade e dedicação no que vem realizando para garantir a satisfação de seus clientes que buscam por Container para escritório em Recife.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>