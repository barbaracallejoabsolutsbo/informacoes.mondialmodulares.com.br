<?php
$title       = "Container 6 metros em Umbaúba - Sergipe";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Para se tornar a melhor empresa que realiza a venda de Container 6 metros em Umbaúba - Sergipe, a Mondial Modulares sempre esteve comprometida em atender às expectativas dos clientes, por isso contamos com colaboradores altamente qualificados que prestarão um atendimento e serviço de excelente qualidade, entre em contato agora para conhecer nossa empresa, nossos produtos e serviços.</p>
<p>Você procura por Container 6 metros em Umbaúba - Sergipe? Contar com empresas especializadas no segmento de Modulares é sempre a melhor saída, já que assim temos a segurança de excelência e profissionalismo. Pensando assim, a empresa Mondial Modulares é a opção certa para quem busca a soma de qualidade, comprometimento e agilidade em Venda de containers, Construção em container, Preço de container, Container pequeno e Aluguel de container para obra e ainda, um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>